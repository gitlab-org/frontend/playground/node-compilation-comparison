#!/bin/bash
set -euo pipefail
IFS=$'\n\t'
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
OUT="$SCRIPT_DIR/out.log"

source "./shared.sh"

rm -f "$OUT"
touch "$OUT"

checkout_and_change_dir

echo "Benchmarking jest"

echo "node version $(node --version)"

rm -rf tmp/cache >>"$OUT" 2>&1
rm -rf node_modules >>"$OUT" 2>&1
yarn clean >>"$OUT" 2>&1
yarn install --frozen-lockfile >>"$OUT" 2>&1
echo "jest run:"
export CI_NODE_INDEX=1
export CI_NODE_TOTAL=10
time node_modules/.bin/jest --runInBand --config jest.config.js --ci --testSequencer ./scripts/frontend/parallel_ci_sequencer.js >>"$OUT" 2>&1