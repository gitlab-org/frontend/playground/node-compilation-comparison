
function checkout_and_change_dir {
REF=${GITLAB_REF:-master}

if ! test -d "gitlab-${REF}" ; then
    echo "Downloading gitlab-org/gitlab on REF=$REF"
    curl -O --silent "https://gitlab.com/gitlab-org/gitlab/-/archive/${REF}/gitlab-${REF}.tar.gz"
    tar -xzf "gitlab-${REF}.tar.gz"
    rm -f "gitlab-${REF}.tar.gz"
fi

cd "gitlab-${REF}"
}