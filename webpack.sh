#!/bin/bash
set -euo pipefail
IFS=$'\n\t'
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
OUT="$SCRIPT_DIR/out.log"

source "./shared.sh"

rm -f "$OUT"
touch "$OUT"

checkout_and_change_dir

echo "Benchmarking webpack"

echo "node version $(node --version)"
rm -rf tmp/cache >>"$OUT" 2>&1
rm -rf node_modules >>"$OUT" 2>&1
yarn clean >>"$OUT" 2>&1
yarn install --frozen-lockfile >>"$OUT" 2>&1
yarn run clean >>"$OUT" 2>&1
echo "Cold cache webpack run:"
time yarn run webpack-prod >>"$OUT" 2>&1
echo "Hot cache webpack run:"
time yarn run webpack-prod >>"$OUT" 2>&1